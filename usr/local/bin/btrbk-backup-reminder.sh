#!/bin/bash

# File to store last run timestamp
TIMESTAMP_FILE="/var/tmp/btrbk_last_run"

# If the file exists, get the timestamp
if [ -f "$TIMESTAMP_FILE" ]; then
    LAST_RUN=$(stat -c %Y "$TIMESTAMP_FILE")
else
    # If the file doesn't exist, create it with the current timestamp
    LAST_RUN=$(date +%s)
    touch "$TIMESTAMP_FILE"
fi

# Current time in seconds
NOW=$(date +%s)

# Threshold for notification (3 days in seconds)
THRESHOLD=$((3 * 24 * 60 * 60))

# Calculate time difference in seconds
DIFF=$((NOW - LAST_RUN))

# If it has been more than 3 days since last run, send notification
if [ $DIFF -ge $THRESHOLD ]; then
    notify-send -t 0 "Backup Reminder" "It has been $((DIFF / 86400)) days since btrfs-backup has been run."
else
    echo "It has been $((DIFF / 86400)) days since btrfs-backup has been run."
fi

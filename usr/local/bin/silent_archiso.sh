#!/bin/bash

# Exit on errors
set -e

# Check for required packages
check_package() {
    pacman -Qq "$1" >/dev/null 2>&1 || {
        echo "$1 is not installed."
        exit 1
    }
}
check_package libisoburn
check_package mtools

# Check if the ISO file has been provided as an argument
if [ "$#" -ne 1 ]; then
    echo "Usage: $0 <path_to_iso>"
    exit 1
fi

ISO_FILE="$1"
ISO_NAME=$(basename "$ISO_FILE" .iso)
TEMP_DIR=$(mktemp -d)

# Extract boot images and loader.conf
osirrox -indev "$ISO_FILE" -extract_boot_images "$TEMP_DIR" -extract /loader/loader.conf "$TEMP_DIR/loader.conf"

# Make loader.conf writable and remove the beep option
chmod +w "$TEMP_DIR/loader.conf"
sed '/^beep on/d' -i "$TEMP_DIR/loader.conf"

# Add the modified loader.conf to the El Torito UEFI boot image
mcopy -D oO -i "$TEMP_DIR/eltorito_img2_uefi.img" "$TEMP_DIR/loader.conf" ::/loader/

# Repack the ISO using the modified boot image and loader.conf
xorriso -indev "$ISO_FILE" \
    -outdev "${ISO_NAME}-silent.iso" \
    -map "$TEMP_DIR/loader.conf" /loader/loader.conf \
    -boot_image any replay \
    -append_partition 2 0xef "$TEMP_DIR/eltorito_img2_uefi.img"

# Clean up temporary files
rm -rf "$TEMP_DIR"

echo "Repacked ISO created: ${ISO_NAME}-silent.iso"

#!/bin/bash

# Specify where the logs should be stored
logs_dir="/home/$USER/Applications/version_logs"

# Ensure logs directory exists
mkdir -p "$logs_dir"

# Function to send notifications with retries
send_notification() {
    local title=$1
    local message=$2
    local retries=5
    local delay=2

    for ((i=0; i<$retries; i++)); do
# https://gitlab.gnome.org/GNOME/libnotify/-/issues/54
#        notify-send -t 0 "$title" "$message" && break
        notify-send -t 600000 "$title" "$message" && break
        sleep $delay
    done
}

# Function to check and notify updates for darktable
check_darktable_update() {
    local log_file="$logs_dir/darktable_version.log"

    # Loop to prevent running with an empty latest_release
    local latest_release=""
    while true; do
        # Get the latest release version from GitHub
        latest_release=$(curl -s "https://api.github.com/repos/darktable-org/darktable/releases/latest" | jq -r '.tag_name' | sed 's/release-//')
        # Wait five minutes if latest_release is empty
        if [ -z "$latest_release" ]; then
            sleep 300
        else
            break
        fi
    done

    # Check if log file exists and compare versions
    if [ -f "$log_file" ]; then
        local logged_version=$(cat "$log_file")
        if [ "$latest_release" != "$logged_version" ]; then
            send_notification "Darktable Update Available" "Version $latest_release of darktable is available."
        fi
    else
        send_notification "Version Log Not Found" "No version log found for darktable. You may need to update the installation script."
    fi
}

# Function to check and notify updates for Joplin
check_joplin_update() {
    local log_file="$logs_dir/joplin_version.log"

    # Loop to prevent running with an empty latest_release
    local latest_release=""
    while true; do
        # Get the latest release version for Joplin
        latest_release=$(curl -s "https://api.github.com/repos/laurent22/joplin/releases/latest" | jq -r '.tag_name' | sed 's/v//')
        
        # Wait five minutes if latest_release is empty
        if [ -z "$latest_release" ]; then
            sleep 300
        else
            break
        fi
    done

    # Check if log file exists and compare versions
    if [ -f "$log_file" ]; then
        local logged_version=$(cat "$log_file")
        if [ "$latest_release" != "$logged_version" ]; then
            send_notification "Joplin Update Available" "Version $latest_release of Joplin is available."
        fi
    else
        send_notification "Version Log Not Found" "No version log found for Joplin. You may need to update the installation script."
    fi
}

# Function to check and notify updates for Librewolf
check_librewolf_update() {
    local log_file="$logs_dir/librewolf_version.log"

    # Loop to prevent running with an empty latest_release
    local latest_release=""
    while true; do
        # Get the latest release version for Librewolf
        latest_release=$(curl -s "https://gitlab.com/api/v4/projects/24386000/releases" | jq -r '.[0].tag_name' | sed 's/^v//')
        
        # Wait five minutes if latest_release is empty
        if [ -z "$latest_release" ]; then
            sleep 300
        else
            break
        fi
    done

    # Check if log file exists and compare versions
    if [ -f "$log_file" ]; then
        local logged_version=$(cat "$log_file")
        if [ "$latest_release" != "$logged_version" ]; then
            send_notification "Librewolf Update Available" "Version $latest_release of Librewolf is available."
        fi
    else
        send_notification "Version Log Not Found" "No version log found for Librewolf. You may need to update the installation script."
    fi
}

# Function to check and notify updates for OnlyOffice
check_onlyoffice_update() {
    local log_file="$logs_dir/onlyoffice_version.log"

    # Loop to prevent running with an empty latest_release
    local latest_release=""
    while true; do
        # Get the latest release version for OnlyOffice
        latest_release=$(curl -s "https://api.github.com/repos/ONLYOFFICE/appimage-desktopeditors/releases/latest" | jq -r '.tag_name' | sed 's/v//')
        
        # Wait five minutes if latest_release is empty
        if [ -z "$latest_release" ]; then
            sleep 300
        else
            break
        fi
    done

    # Check if log file exists and compare versions
    if [ -f "$log_file" ]; then
        local logged_version=$(cat "$log_file")
        if [ "$latest_release" != "$logged_version" ]; then
            send_notification "OnlyOffice Update Available" "Version $latest_release of OnlyOffice is available."
        fi
    else
        send_notification "Version Log Not Found" "No version log found for OnlyOffice. You may need to update the installation script."
    fi
}

# Function to check and notify updates for Upscayl
check_upscayl_update() {
    local log_file="$logs_dir/upscayl_version.log"

    # Loop to prevent running with an empty latest_release
    local latest_release=""
    while true; do
        # Get the latest release version for Upscayl
        latest_release=$(curl -s "https://api.github.com/repos/upscayl/upscayl/releases/latest" | jq -r '.tag_name' | sed 's/v//')
        
        # Wait five minutes if latest_release is empty
        if [ -z "$latest_release" ]; then
            sleep 300
        else
            break
        fi
    done
    
    # Check if log file exists and compare versions
    if [ -f "$log_file" ]; then
        local logged_version=$(cat "$log_file")
        if [ "$latest_release" != "$logged_version" ]; then
            send_notification "Upscayl Update Available" "Version $latest_release of Upscayl is available."
        fi
    else
        send_notification "Version Log Not Found" "No version log found for Upscayl. You may need to update the installation script."
    fi
}

# Function to check and notify updates for Krita
check_krita_update() {
    local log_file="$logs_dir/krita_version.log"

    # Loop to prevent running with an empty latest_release
    local latest_release=""
    while true; do
        # Get the latest release version for Krita
        local base_url="https://download.kde.org/stable/krita/"
        latest_release=$(curl -s "$base_url" | grep -oP 'href="\K[0-9]+\.[0-9]+\.[0-9]+(?=/)' | sort -V | tail -n 1)
        
        # Wait five minutes if latest_release is empty
        if [ -z "$latest_release" ]; then
            sleep 300
        else
            break
        fi
    done
    
    # Check if log file exists and compare versions
    if [ -f "$log_file" ]; then
        local logged_version=$(cat "$log_file")
        if [ "$latest_release" != "$logged_version" ]; then
            send_notification "Krita Update Available" "Version $latest_release of Krita is available."
        fi
    else
        send_notification "Version Log Not Found" "No version log found for Krita. You may need to update the installation script."
    fi
}

# Call update check functions for each application
check_darktable_update
check_joplin_update
check_librewolf_update
check_onlyoffice_update
check_upscayl_update
check_krita_update

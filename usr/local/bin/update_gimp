#!/bin/bash

# Log file path
log_file="/home/$USER/Applications/version_logs/gimp_version.log"

# Let it rip
echo "Updating GIMP AppImage..."

# Check if the AppImage file exists
if [ -f "/home/$USER/Applications/GIMP.AppImage" ]; then
    # Move current AppImage to backup
    echo "Backing up current version..."
    mv "/home/$USER/Applications/GIMP.AppImage" "/home/$USER/Applications/GIMP.AppImage.old"
fi

# Extract download URL for the AppImage
download_url=$(curl -s https://api.github.com/repos/ivan-hc/GIMP-appimage/releases/tags/continuous-stable | grep -o 'https://github.com/ivan-hc/GIMP-appimage/releases/download/continuous-stable/GNU-Image-Manipulation-Program_[^"]*\.AppImage' | head -n 1)

# Determine latest release for version log
latest_release=$(echo "$download_url" | sed 's/.*GNU-Image-Manipulation-Program_//' | sed 's/\.AppImage.*//')

# Download the latest release
curl -L -o "/home/$USER/Applications/GIMP.AppImage" "$download_url"

# Check if download was successful
if [ $? -eq 0 ]; then
    echo "Download complete."
    chmod +x "/home/$USER/Applications/GIMP.AppImage"
    echo "GIMP AppImage updated successfully."
    # Log the latest release version
    echo "$latest_release" > "$log_file"
else
    echo "Failed to download the latest version:"
    echo "$curl_result"
    # Restore previous version if it was backed up
    if [ -f "/home/$USER/Applications/GIMP.AppImage.old" ]; then
        echo "Restoring previous version..."
        mv "/home/$USER/Applications/GIMP.AppImage.old" "/home/$USER/Applications/GIMP.AppImage"
    fi
    echo "Update failed."
fi

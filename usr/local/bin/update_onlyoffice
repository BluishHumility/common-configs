#!/bin/bash

# Log file path
log_file="/home/$USER/Applications/version_logs/onlyoffice_version.log"

# Let it rip
echo "Updating OnlyOffice AppImage..."

# Check if the AppImage file exists
if [ -f "/home/$USER/Applications/OnlyOffice.AppImage" ]; then
    # Move current AppImage to backup
    echo "Backing up current version..."
    mv "/home/$USER/Applications/OnlyOffice.AppImage" "/home/$USER/Applications/OnlyOffice.AppImage.old"
fi

# Download the latest release
echo "Downloading the latest version..."
latest_release=$(curl -s "https://api.github.com/repos/ONLYOFFICE/appimage-desktopeditors/releases/latest" | jq -r '.tag_name' | sed 's/v//')
curl_result=$(curl -L -o "/home/$USER/Applications/OnlyOffice.AppImage" "https://github.com/ONLYOFFICE/appimage-desktopeditors/releases/download/v${latest_release}/DesktopEditors-x86_64.AppImage" 2>&1)

# Check if download was successful
if [ $? -eq 0 ]; then
    echo "Download complete."
    chmod +x "/home/$USER/Applications/OnlyOffice.AppImage"
    echo "OnlyOffice AppImage updated successfully."
    # Log the latest release version
    echo "$latest_release" > "$log_file"
else
    echo "Failed to download the latest version:"
    echo "$curl_result"
    # Restore previous version if it was backed up
    if [ -f "/home/$USER/Applications/OnlyOffice.AppImage.old" ]; then
        echo "Restoring previous version..."
        mv "/home/$USER/Applications/OnlyOffice.AppImage.old" "/home/$USER/Applications/OnlyOffice.AppImage"
    fi
    echo "Update failed."
fi

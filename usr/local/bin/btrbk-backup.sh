#!/bin/bash

# Command to run btrbk backup
btrbk -c /etc/btrbk/btrbk-backup.conf -v run

# Update timestamp file
touch /var/tmp/btrbk_last_run

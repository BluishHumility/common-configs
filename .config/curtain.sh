#!/bin/bash

echo "Decrypting archive..."
gpg -o ~/.config/curtain.tar.gz -d ~/.config/curtain.tar.gz.gpg
# Extract the archive
echo "Extracting archive..."
tar xzf ~/.config/curtain.tar.gz -C ~/.config/
# Distribute the curtain files
echo "Distributing curtain files..."
rsync -av --ignore-existing --info=skip ~/.config/curtain/.config/ ~/.config/
rsync -av --ignore-existing --info=skip --exclude='*/' ~/.config/curtain/ ~/

# Function to prompt for each setup item and conditionally proceed with the setup
prompt_setup() {
    local question="$1"
    local command="$2"

    read -p "$question (y/n): " response
    if [[ "$response" == "y" ]]; then
        eval "$command"
    fi
}

# Yubikey
prompt_setup "Set up Yubikey?" "mkdir -p ~/.config/Yubico && pamu2fcfg > ~/.config/Yubico/u2f_keys"

# Fingerprint signature
prompt_setup "Set up fingerprint signature?" "fprintd-enroll && fprintd-verify"

# Calcurse
read -p "Set up Calcurse? (y/n): " response
if [[ "$response" == "y" ]]; then
    micro ~/.config/calcurse/caldav/config
    calcurse-caldav --init=keep-remote
fi

# Browser
read -p "Set up the browser? (y/n): " response
if [[ "$response" == "y" ]]; then
echo "Update identity.sync.tokenserver.uri in about:config."
    read -n 1 -s -r -p "Press any key to continue..."
    echo
    echo "Set up Syncthing - use YunoHost connection."
    read -n 1 -s -r -p "Press any key to continue..."
    echo
    echo "Find user folder in about:support."
    read -n 1 -s -r -p "Press any key to continue..."
    echo
    read -p "User folder: " librewolf_user_folder
    echo "cp ~/Sync/files/librewolf_permissions.sqlite ${librewolf_user_folder}/permissions.sqlite"
fi

# Tailscale
read -p "Configure Tailscale? (y/n): " response
if [[ "$response" == "y" ]]; then
    sudo tailscale up
    sudo tailscale up --exit-node=blueduck --exit-node-allow-lan-access --accept-routes
fi

# SSH key
read -p "Generate new SSH key and config? (y/n): " response
if [[ "$response" == "y" ]]; then
    ssh-keygen -C "$(whoami)@$(uname -n)-$(date -I)"
    read -p "Send SSH key to remote server? (y/n): " response
    if [[ "$response" == "y" ]]; then
        # Generate ssh config
        echo "Generating ssh config..."
        cat <<'EOF' > "~/.ssh/config"
# Make all ssh clients store keys in the agent on first use
AddKeysToAgent yes

# Switch to Fish on Blueduck
Host blueduck
	HostName 192.168.0.2
	RequestTTY yes
	RemoteCommand fish
EOF
        # Loop to add SSH key to remote servers
        while true; do
            read -p "Enter user@ip value for remote server: " remote_server
            ssh-copy-id "${remote_server}"
            read -p "Add another server? (y/n): " add_another
            if [[ "$add_another" != "y" ]]; then
                break  # Exit the loop when we are done adding servers
            fi
        done
    fi
    echo "Don't forget to add the key to GitLab and GitHub if needed."
fi

# btrbk
read -p "Set up btrbk? (y/n): " response
if [[ "$response" == "y" ]]; then
    read -p "Enter machine (shared) ID: " machine_id
    read -p "Enter hostname or device ID: " device_id
    read -p "Enter remote server IP address: " server_ip
    # Generate SSH key
    echo "Generating an SSH key..."
    sudo mkdir /etc/btrbk/ssh
    sudo ssh-keygen -t ed25519 -C "btrbk@$(uname -n)-$(date -I)" -f /etc/btrbk/ssh/id_ed25519 -N ""
    # Send SSH key to the server
    echo "Sending key to the server..."
    sudo ssh-copy-id -i /etc/btrbk/ssh/id_ed25519.pub "root@${server_ip}"
    # Make a directory for the backups of this machine on the server if it doesn't already exist.
    # Nest directories for device_id home subvolume and the shared subvolumes inside the machine directory.
    echo "Setting up backup directories on remote server..."
    sudo  ssh -i /etc/btrbk/ssh/id_ed25519 "root@${server_ip}" "mkdir -p /mnt/btrbk_backups/$machine_id/{Documents,Music,Pictures,Sync,Videos,$device_id}"
    # Make a directory in _btrbk_snap if it doesn't already exist.
    sudo mkdir -p "/_btrbk_snap/${device_id}" 
    # Backup the configs and replace the placeholder values
    echo "Updating btrbk-backup.conf and btrbk-snapshot.conf..."
    sudo sed -i.bak -e "s|device_id_placeholder|${device_id}|" \
                    -e "s|machine_id_placeholder|${machine_id}|" \
                    -e "s|server_ip_placeholder|${server_ip}|" /etc/btrbk/btrbk-backup.conf
    sudo sed -i.bak -e "s|device_id_placeholder|${device_id}|" /etc/btrbk/btrbk-snapshot.conf

read -p "Setup complete. Remove curtain resources? (y/n): " response
if [[ "$response" == "y" ]]; then
    rm -r ~/.config/curtain*
fi

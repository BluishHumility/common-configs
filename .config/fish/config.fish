## Set values
# Hide welcome message
set fish_greeting
set VIRTUAL_ENV_DISABLE_PROMPT "1"

# Report shell as Fish
set -x SHELL /usr/bin/fish

# Man pages
set -x MANROFFOPT "-c"
set -x MANPAGER "sh -c 'col -bx | bat -l man -p'"

# Add ~/.nix-profile/bin to PATH
if test -d ~/.nix-profile/bin
    if not contains -- ~/.nix-profile/bin $PATH
        set -p PATH ~/.nix-profile/bin
    end
end

# Starship
starship init fish | source

## Useful aliases
alias ls 'eza -alg --color=always --group-directories-first --icons --smart-group'
alias lsz 'eza -alg --color=always --total-size --group-directories-first --icons --smart-group'
alias lt 'eza -aT --color=always --group-directories-first --icons' # tree listing
alias ip "ip -color"
alias cat 'bat --style header --style snip --style changes --style header'
alias tarnow 'tar -acf '
alias untar 'tar -xvf '
alias wget 'wget -c '
alias grep 'grep --color=auto'
alias hw 'hwinfo --short'
alias tb 'nc termbin.com 9999'
alias pacdiff 'sudo -H DIFFPROG=meld pacdiff'
alias lf lfcd

# Get the error messages from journalctl
alias jctl 'journalctl -p 3 -xb'

# Set vconsole colors
set_vconsole_colors

## What is this?

This is a repo with some configuration files I use in my custom installation routines (https://gitlab.com/BluishHumility/arch-i3, https://gitlab.com/BluishHumility/eos-sway, https://gitlab.com/BluishHumility/garuda-setup).

## Why did you make this?

A few of my repos had identical copies of directories and configs. When I would add, remove, or change something I would have to push the same commit to all of the relevant repos.

The purpose of this repo is to hold configs which are common to all the installation routines. Then, each installation routine can pull from this repo instead of needing to maintain multiple sets of identical configs.

---

The author and source of the repo avatar icon: <a href="https://www.freepik.com/icons/configuration">Icon by Febrian Hidayat</a>